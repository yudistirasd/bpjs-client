<?php

namespace Yudistira\BPJSClient;

use LZCompressor\LZString;

trait Decryptor
{
    public static function decrypt($string, $key)
    {
        $stringDecrypted = self::stringDecrypt($key, $string);
        return self::decompress($stringDecrypted);
    }

    public static function stringDecrypt($key, $string)
    {
        $keyHash = hex2bin(hash('sha256', $key));

        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        $output = openssl_decrypt(base64_decode($string), 'AES-256-CBC', $keyHash, OPENSSL_RAW_DATA, $iv);

        return $output;
    }

    public static function decompress($string)
    {
        return LZString::decompressFromEncodedURIComponent($string);
    }
}

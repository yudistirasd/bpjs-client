<?php

namespace Yudistira\BPJSClient;

trait Signature
{
    public function generate($consId, $secretKey)
    {
        $signature = hash_hmac('sha256', $consId . "&" . $this->timestamps(), $secretKey, true);

        $encodedSignature = base64_encode($signature);

        return $encodedSignature;
    }

    public function timestamps()
    {
        date_default_timezone_set('UTC');

        $timestamp = strval(time() - strtotime('1970-01-01 00:00:00'));

        date_default_timezone_set('Asia/Jakarta');

        return $timestamp;
    }
}

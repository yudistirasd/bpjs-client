<?php

namespace Yudistira\BPJSClient;

use Log;
use Yudistira\BPJSClient\ClientException;
use Yudistira\BPJSClient\Decryptor;
use Yudistira\BPJSClient\Signature;
use Yudistira\BPJSClient\Http;
use GuzzleHttp\Client;
use Dotenv\Dotenv;

class Antrean
{
    use Signature, Decryptor, Http;

    protected $client, $headers;

    public function __construct()
    {
        $dotenv = Dotenv::createUnsafeImmutable(__DIR__);
        $dotenv->required(['BPJS_ANTREAN_ENDPOINT', 'BPJS_ANTREAN_CONS_ID', 'BPJS_ANTREAN_SECRET_KEY', 'BPJS_ANTREAN_USER_KEY']);
        $dotenv->safeLoad();

        $this->client = new Client(['verify', true, 'cookie' => false]);
        $this->headers = $this->prepareHeader();
    }

    public function setServiceApi()
    {
        return getenv('BPJS_ANTREAN_ENDPOINT');
    }

    public function setConsID()
    {
        return getenv('BPJS_ANTREAN_CONS_ID');
    }

    public function setSecretKey()
    {
        return getenv('BPJS_ANTREAN_SECRET_KEY');
    }

    public function setUserKey()
    {
        return getenv('BPJS_ANTREAN_USER_KEY');
    }

    public function setSignature()
    {
        return $this->generate($this->setConsID(), $this->setSecretKey());
    }

    public function setKeyDecrypt($timestamps)
    {
        return $this->setConsId() . $this->setSecretKey() . $timestamps;
    }

    public function prepareHeader()
    {
        return [
            'X-cons-id' => $this->setConsID(),
            'X-timestamp' => null,
            'X-signature' => $this->setSignature(),
            'user_key'    => $this->setUserKey(),
            'Accept' => 'application/json',
            'Content-Type' => 'Application/x-www-form-urlencoded'
        ];
    }

    public function prepareResponse($result, $timestamps)
    {
        try {
            $result = json_decode($result->getBody()->getContents());

            if ($result->metadata->code != 200) {
                throw new ClientException("Error Antrean RS : {$result->metadata->message}", $result->metadata->code);
            }

            $decryptedResponse = $this->decrypt($result->response, $this->setKeyDecrypt($timestamps));

            $decryptedResponse  = json_decode($decryptedResponse);

            return json_encode([
                'metaData' => $result->metadata,
                'response' => $decryptedResponse
            ]);
        } catch (\Exception $ex) {
            Log::error("Antrean Service : " . $ex->getMessage());
            throw $ex;
        }
    }
}

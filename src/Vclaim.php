<?php

namespace Yudistira\BPJSClient;

use Log;
use Yudistira\BPJSClient\ClientException;
use Yudistira\BPJSClient\Decryptor;
use Yudistira\BPJSClient\Signature;
use Yudistira\BPJSClient\Http;
use GuzzleHttp\Client;
use Dotenv\Dotenv;

class Vclaim
{
    use Signature, Decryptor, Http;

    protected $client, $headers;

    public function __construct()
    {
        $dotenv = Dotenv::createUnsafeImmutable(__DIR__);
        $dotenv->required(['BPJS_VCLAIM_ENDPOINT', 'BPJS_VCLAIM_CONS_ID', 'BPJS_VCLAIM_SECRET_KEY', 'BPJS_VCLAIM_USER_KEY']);
        $dotenv->safeLoad();

        $this->client = new Client(['verify', false, 'cookie' => false]);
        $this->headers = $this->prepareHeader();
    }

    public function setServiceApi()
    {
        return getenv('BPJS_VCLAIM_ENDPOINT');
    }

    public function setConsID()
    {
        return getenv('BPJS_VCLAIM_CONS_ID');
    }

    public function setSecretKey()
    {
        return getenv('BPJS_VCLAIM_SECRET_KEY');
    }

    public function setUserKey()
    {
        return getenv('BPJS_VCLAIM_USER_KEY');
    }

    public function setSignature()
    {
        return $this->generate($this->setConsID(), $this->setSecretKey());
    }

    public function setKeyDecrypt($timestamps)
    {
        return $this->setConsId() . $this->setSecretKey() . $timestamps;
    }

    public function prepareHeader()
    {
        return [
            'X-cons-id' => $this->setConsID(),
            'X-timestamp' => null,
            'X-signature' => $this->setSignature(),
            'user_key'    => $this->setUserKey(),
            'Accept' => 'application/json',
        ];
    }

    public function prepareResponse($result, $timestamps)
    {
        try {

            $result = json_decode($result->getBody()->getContents());

            if ($result->metaData->code != 200) {
                throw new ClientException("Error VClaim : {$result->metaData->message}", $result->metaData->code);
            }

            $decryptedResponse = $this->decrypt($result->response, $this->setKeyDecrypt($timestamps));

            $decryptedResponse  = json_decode($decryptedResponse);

            return json_encode([
                'metaData' => $result->metaData,
                'response' => $decryptedResponse
            ]);
        } catch (\Exception $ex) {
            Log::error("Vclaim service : " . $ex->getMessage());
            //throw $ex;
            return json_encode([
                'metaData' => $result->metaData
                //'response' => $decryptedResponse
            ]);
        }
    }
}

<?php

namespace Yudistira\BPJSClient;

trait Http
{

    private function setHeaders($headers)
    {
        return array_merge($this->headers, $headers);
    }

    public function get($endPoint, $headers = [])
    {
        try {
            $url = $this->setServiceApi() . $endPoint;

            $timestamps = $this->timestamps();

            $headers = $this->setHeaders($headers);
            $headers['X-timestamp'] = $timestamps;

            $response = $this->client->get($url, ['headers' => $headers]);

            return $this->prepareResponse($response, $timestamps);
        } catch (\Exception $ex) {
            return response()->json([
                'code' => $ex->getCode(),
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function post($endPoint, $body, $headers = [])
    {
        try {
            $url = $this->setServiceApi() . $endPoint;

            $timestamps = $this->timestamps();

            $headers = $this->setHeaders($headers);
            $headers['Content-Type'] = 'Application/x-www-form-urlencoded';
            $headers['X-timestamp'] = $timestamps;

            $response = $this->client->post($url, ['headers' => $this->setHeaders($headers), 'body' => $body]);

            return $this->prepareResponse($response, $timestamps);
        } catch (\Exception $ex) {
            return response()->json([
                'code' => $ex->getCode(),
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function put($endPoint, $body, $headers = [])
    {
        try {
            $url = $this->setServiceApi() . $endPoint;

            $timestamps = $this->timestamps();

            $headers = $this->setHeaders($headers);
            $headers['Content-Type'] = 'Application/x-www-form-urlencoded';
            $headers['X-timestamp'] = $timestamps;

            $response = $this->client->put($url, ['headers' => $this->setHeaders($headers), 'body' => $body]);

            return $this->prepareResponse($response, $timestamps);
        } catch (\Exception $ex) {
            return response()->json([
                'code' => $ex->getCode(),
                'message' => $ex->getMessage()
            ]);
        }
    }

    public function delete($endPoint, $body, $headers = [])
    {
        try {
            $url = $this->setServiceApi() . $endPoint;

            $timestamps = $this->timestamps();

            $headers = $this->setHeaders($headers);
            $headers['Content-Type'] = 'Application/x-www-form-urlencoded';
            $headers['X-timestamp'] = $timestamps;

            $response = $this->client->delete($url, ['headers' => $this->setHeaders($headers), 'body' => $body]);

            return $this->prepareResponse($response, $timestamps);
        } catch (\Exception $ex) {
            return response()->json([
                'code' => $ex->getCode(),
                'message' => $ex->getMessage()
            ]);
        }
    }
}
